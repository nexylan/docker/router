compose=docker-compose \
	--file docker-compose.dev.yml \
	--file docker-compose.yml

all: build up ps

build:
	$(compose) build --parallel

up:
	$(compose) up --detach

ps:
	$(compose) ps

test:
	$(compose) run test

clean:
	$(compose) down --remove-orphans
