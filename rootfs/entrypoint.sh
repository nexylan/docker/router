#!/bin/sh
set -e
# source: https://github.com/containous/traefik-library-image/blob/v2.0.2/alpine/entrypoint.sh

traefik version

set -- traefik \
	--global.sendAnonymousUsage \
	--log.level=INFO \
	--api \
	--accessLog \
	--entrypoints.web.address=:"${NEXY_ROUTER_HTTP_PORT:-80}" \
	--entrypoints.web-secure.address=:"${NEXY_ROUTER_HTTPS_PORT:-443}" \
	--certificatesResolvers.letsencrypt.acme.email="${NEXY_ACME_EMAIL:-postmaster@nexylan.com}" \
	--certificatesResolvers.letsencrypt.acme.storage=/data/acme.json \
	--certificatesResolvers.letsencrypt.acme.httpChallenge.entryPoint=web \
	--providers.file.directory=/etc/traefik \
	--providers.docker \
	--providers.docker.exposedByDefault=false \
	"$@"

exec "$@"
