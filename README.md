# Router

On steroid traffic router.

## Requirements

- Docker

## Usage

The usage is exactly the same as the official Traefik image with cli flags:

```yaml
version: "3.7"

services:
  router:
    image: registry.gitlab.com/nexylan/docker/router:x.y.z
    restart: always
    labels:
      # Specify the domain to use to access to the router dashboard.
      # More info: https://docs.traefik.io/routing/routers/
      - traefik.http.routers.api.rule=Host(`router.domain.com`)
      # Add some security rules. In this example, the router is protected with admin:test credentials.
      # More info: https://docs.traefik.io/middlewares/overview/
      # Basic auth user generation: https://docs.traefik.io/middlewares/basicauth/#configuration-examples
      - "traefik.http.middlewares.internal.basicauth.users=admin:$$apr1$$2Pzbr49d$$VyiOhKHkYLKJMDBBTEN5l0"
      - traefik.http.routers.api.middlewares=recommended@file,internal@docker
    volumes:
      - router:/data
      - /var/run/docker.sock:/var/run/docker.sock:ro
    ports:
      - "80:80"
      - "443:443"

volumes:
  router: ~

networks:
  default:
    name: your_company
```

Some important notes about the configuration example:

- `traefik.http.routers.api.[...]`: Internal dashboard related router. You have add your configuration such as rules or middleware on this namespace.
- `router:/data`: Traefik data to be persisted. As an example, it contains the Let's Encrypt certificates' data.
- `/var/run/docker.sock:/var/run/docker.sock:ro`: Only needed for the docker provider.
- `networks.default.name`: You must name a network to attach your container on it. So Traefik can detect and link them.

Default flags are defined in our [custom entrypoint](./rootfs/entrypoint.sh).

You can override or add any flags by putting them on the `command` section of you service.
Please see the [official documentation][traefik_docs_reference_config_cli] to list the available flags.

ℹ️ Environment variables configuration will not work because of cli flags usage.

⚠️ **DO NOT** use any `traefik.{toml,yaml,yml}` file with this image. It will **supersede** the given flag options.

### Custom configuration file

If you need to add configurations that are not available through docker labels,
the simplest way is to bind a volume to your config folder:

```yaml
services:
  router:
    image: registry.gitlab.com/nexylan/docker/router:x.y.z
    volumes:
      # With a single file
      - ./conf/my_traefik.toml:/etc/traefik/my_traefik.toml:ro
```

⚠️ You MAY NOT want to bind an entire configuration folder (e.g. `./conf/traefik:/etc/traefik:ro`)
because it will erase all the default configuration files.

A maybe more convenient way is to copy the files from the `Dockerfile` of the image:

```Dockerfile
FROM registry.gitlab.com/nexylan/docker/router:x.y.z
# Note: This way, the default file will not be erased.
COPY etc/traefik /etc/traefik
```

The `/etc/traefik` is automatically watched thanks to our default configuration.

To avoid filename conflicts, our configuration file is currently named [nexylan.toml](rootfs/etc/traefik/nexylan.toml).

### Modify entrypoint ports

The router image provides the `web` and `web-secure` entry points
using respectively the port `80` (http) and `443` (https).

If you want to change that, you can simply do it using two environment variables.
The simplest way to use them is to put the defaults on the `.env` file:

```
NEXY_ROUTER_HTTP_PORT=8888
NEXY_ROUTER_HTTPS_PORT=9999
```

And then use them on `docker-compose.yml`:

```yaml
services:
  router:
    # ...
    ports:
      - "80:${NEXY_ROUTER_HTTP_PORT}"
      - "443:${NEXY_ROUTER_HTTPS_PORT}"
    environment:
      - NEXY_ROUTER_HTTP_PORT
      - NEXY_ROUTER_HTTPS_PORT
```

You can also add one or many custom entry points to fit your needs using the command section:

```
services:
  router:
    command:
      - --entrypoints.streaming.address=:1704/udp
```

The option will be added to our default entrypoint command.

More info: https://docs.traefik.io/v2.2/routing/entrypoints/

### Supported Provider

Some providers are pre-activated and configured.
The other providers are still available, but you will have to configure them yourself.

Feel free to contribute if you want to configure an another one!

#### Docker

Docker is enabled by default but your containers **will not** be exposed unless you set the `traefik.enable=true` label:

```yaml
version: "3.7"

services:
  nginx:
    image: nginx
    labels:
      - traefik.enable=true
      - traefik.http.routers.portainer.rule=Host(`www.sample.com`)

networks:
  default:
    external:
      name: your_company
```

See the [official documentation][traefik_docs_provider_docker_options] for more info.

### Let's Encrypt

A default Let's Encrypt certificate resolvers is provided. It name is: `letsencrypt`.

To use it, add the following label:

```yaml
labels:
  # ...
  - traefik.http.routers.portainer.tls.certresolver=letsencrypt
```

You may also want to setup your email to register:

```yaml
environment:
  # ...
  NEXY_ACME_EMAIL: customer@company.com
```

### HTTPS redirects

Any HTTP request is automatically redirected to the HTTPS scheme out of the box.

## Development

```
make
```

[traefik_docs_reference_config_cli]: https://docs.traefik.io/v2.0/reference/static-configuration/cli/ "Traefik CLI configuration reference"
[traefik_docs_provider_docker_options]: https://docs.traefik.io/v2.0/routing/providers/docker/#specific-provider-options "Docker provider specific options"
[docker_compose_env_file]: https://docs.docker.com/compose/environment-variables/#the-env-file
