FROM registry.gitlab.com/nexylan/docker/core:2.2.2 as core
FROM traefik:2.9.8
COPY --from=core / /
RUN setup

VOLUME [ "/data" ]
COPY rootfs /

LABEL traefik.enable=true

# API route configuration
# To make it working, please add the following label:
# traefik.http.routers.api.rule=Host(`router.domain.com`)
LABEL \
  traefik.http.routers.api.tls.certresolver=letsencrypt \
  traefik.http.routers.api.service=api@internal

# HTTP to HTTPS redirect
LABEL \
  traefik.http.routers.web.rule=HostRegexp(`{domain:.*}`) \
  traefik.http.routers.web.entrypoints=web \
  traefik.http.routers.web.middlewares=redirectscheme \
  traefik.http.middlewares.redirectscheme.redirectscheme.scheme=https \
  traefik.http.middlewares.redirectscheme.redirectscheme.permanent=true
